public class Main {
    public static void main(String[] args) {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();
        for (int i = 1; i <= 5; i++) {
            list.addFirst(i);
        }
        list.printList();
        list.addByIndex(2, 10);
        list.printList();
        System.out.println(list.getSize());
        list.setByIndex(3, 100);
        list.printList();
        System.out.println(list.removeFirst());
        list.printList();
        System.out.println(list.removeLast());
        list.printList();
        System.out.println(list.removeByIndex(3));
        list.printList();
    }
}
