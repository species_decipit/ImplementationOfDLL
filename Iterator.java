/**
 * Interface of iterator, which allows to
 * move in both side in DLL
 * Use Generic <T>
 * @param <T>
 */
public interface Iterator<T> {
    /**
     * Check existence of next element
     * @return True or False
     */
    boolean hasNext();


    /**
     * Check existence of previous element
     * @return True or False
     */
    boolean hasPrev();


    /**
     * Return next element
     * @return return a value of the next element
     */
    T next();


    /**
     * Return previous element
     * @return return a value of the previous element
     */
    T prev();
}
