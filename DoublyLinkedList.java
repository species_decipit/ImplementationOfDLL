/**
 * Implementation of Doubly Linked List
 * with iterator and generic
 * @param <T>
 */
public class DoublyLinkedList<T> implements Iterable<T> {
    // Declaration
    private int _size = 0; // Size of list
    private Node<T> _head = null; // Reference to head of list
    private Node<T> _tail = null; // Reference to tail of list


    /**
     * Support iteration: hasNext/hasPrev/next/prev
     * @return iterator of DLL (Iterator<T>)
     */
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> current = new Node<T>(null, null, _head);

            /**
             * Check existence of next element
             * @return return True or False
             */
            @Override
            public boolean hasNext() {
                return current.hasNext();
            }

            /**
             * Check of existence of previous element
             * @return return True or False
             */
            @Override
            public boolean hasPrev() {
                return current.hasPrev();
            }

            /**
             * Move to next element
             * @throws IndexOutOfBoundsException if next element is null
             * @return remove value of the next element (<T>)
             */
            @Override
            public T next() {
                if (!hasNext()) throw new IndexOutOfBoundsException();
                current = current.getNext();
                return current.getValue();
            }

            /**
             * Move to previous element
             * @throws IndexOutOfBoundsException if previous element is null
             * @return remove value of the previous element (<T>)
             */
            @Override
            public T prev() {
                if (!hasPrev()) throw new IndexOutOfBoundsException();
                current = current.getPrev();
                return current.getValue();
            }
        };
    }


    /**
     * Check is list empty or nor
     * @return True or False
     */
    public boolean isEmpty() { return getSize() == 0; }


    /**
     * Add element in the beginning of list
     * @param value value of new element
     */
    public void addFirst(T value) {
        Node<T> node = new Node<T>(value);
        if (isEmpty()) {
            node.setNext(null);
            node.setPrev(null);
            _head = node;
            _tail = node;
        } else {
            node.setNext(_head);
            _head.setPrev(node);
            _head = node;
            _head.setPrev(null);
        }
        _size++;
    }


    /**
     * Add element in the end of list
     * @param value value of new element
     */
    public void addLast(T value) {
        Node<T> node = new Node<T>(value);
        if (isEmpty()) {
            node.setNext(null);
            node.setPrev(null);
            _head = node;
            _tail = node;
        } else {
            _tail.setNext(node);
            node.setPrev(_tail);
            _tail = node;
            _tail.setNext(null);
        }
        _size++;
    }


    /**
     * Add new element in list by index
     * @throws IndexOutOfBoundsException if list is empty or index is out of range
     * @param index index at which element will be placed
     * @param value value of new element
     */
    public void addByIndex(int index, T value) {
        if (isEmpty() || index < 0 || index > getSize() - 1) {
            throw new IndexOutOfBoundsException();
        } else {
            if (index == 0) {
                addFirst(value);
            } else {
                Node<T> node = new Node<T>(value);
                Node<T> current = _head;
                for (int i = 1; i <= index - 1; i++) {
                    current = current.getNext();
                }
                node.setNext(current.getNext());
                node.setPrev(current);
                current.getNext().setPrev(node);
                current.setNext(node);
                _size++;
            }
        }
    }


    /**
     * Remove first element in list
     * @throws IndexOutOfBoundsException if list is empty
     * @return return value of the removed element (<T>)
     */
    public T removeFirst() {
        if (isEmpty()) {
            throw new IndexOutOfBoundsException();
        } else {
            T result = _head.getValue();
            if (getSize() == 1) {
                _head = null;
                _tail = null;
            } else {
                _head = _head.getNext();
                _head.setPrev(null);
            }
            _size--;
            return result;
        }
    }


    /**
     * Remove last in list
     * @throws IndexOutOfBoundsException if list is empty
     * @return return value of the removed element (<T>)
     */
    public T removeLast() {
        if (isEmpty()) {
            throw new IndexOutOfBoundsException();
        } else {
            T result = _tail.getValue();
            if (getSize() == 1) {
                _head = null;
                _tail = null;
            } else {
                _tail = _tail.getPrev();
                _tail.setNext(null);
            }
            _size--;
            return result;
        }
    }


    /**
     * Remove element from list by index
     * @throws IndexOutOfBoundsException if list is empty or index is our if range
     * @param index index of the removed element
     * @return return value of the removed element (<T>)
     */
    public T removeByIndex(int index) {
        if (isEmpty() || index < 0 || index > getSize() - 1) {
            throw new IndexOutOfBoundsException();
        } else {
            if (getSize() == 1) {
                return removeFirst();
            } else if (index == 0) {
                return removeFirst();
            } else if (index == getSize() - 1) {
                return removeLast();
            } else {
                Node<T> current = _head;
                for (int i = 1; i <= index; i++) {
                    current = current.getNext();
                }
                T result = current.getValue();
                current.getPrev().setNext(current.getNext());
                current.getNext().setPrev(current.getPrev());
                _size--;
                return result;
            }
        }
    }


    /**
     * Set a new value of element by index
     * @throws IndexOutOfBoundsException if list is empty or index is out of range
     * @param index index of upgraded element
     * @param value a new value of upgraded element
     */
    public void setByIndex(int index, T value) {
        if (isEmpty() || index < 0 || index > getSize() - 1) {
            throw new IndexOutOfBoundsException();
        } else {
            Node<T> current = _head;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            current.setValue(value);
        }
    }


    /**
     * Return a value of element by index
     * @throws IndexOutOfBoundsException if list is empty or index is out of range
     * @param index index of element which we want to get
     * @return value of element (<T>)
     */
    public T getByIndex(int index) {
        if (isEmpty() || index < 0 || index > getSize() - 1) {
            throw new IndexOutOfBoundsException();
        } else {
            Node<T> current = _head;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return current.getValue();
        }
    }


    /**
     * Return size of list
     * @return return size (int)
     */
    public int getSize() { return _size; }


    /**
     * Print list in console
     */
    public void printList() {
        Iterator<T> iterator = iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.print("\n");
    }
}
