/**
 * Interface which allows some class be iterable
 * @param <T>
 */
public interface Iterable<T> {
    /**
     * Return iterator
     * @return return iterator on Data Structure
     */
    Iterator<T> iterator();
}
